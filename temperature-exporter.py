#!/usr/bin/env python3

import re
import time
import argparse
import subprocess
import os
import sys
import logging
import shlex

from prometheus_client.core import GaugeMetricFamily
from prometheus_client import make_wsgi_app
from wsgiref.simple_server import make_server, WSGIRequestHandler

VERSION = "0.0.6"

class ParsingError(Exception):
    pass


class InfinibandCollector(object):
    def __init__(self, lid_to_name_map):
        self.lid_to_name_map = lid_to_name_map

        self.lid_to_name = {}
        if self.lid_to_name_map:
            with open(self.lid_to_name_map) as f:
                for line in f:
                    m = re.search(r'(?P<LID>0x.*)\s+"(?P<GUID>.*)"', line)
                    if m:
                        self.lid_to_name[m.group(1)] = m.group(2)

        self.scrape_with_errors = False
        self.metrics = {}

        self.device_temp = {
            'device_temp': {
                'help': 'Device current temperature.',
            }
        }
        self.link_temp = {
            'Link_Temp': {
                'help': 'Link current temperature.',
            }
        }

        self.identify_link_temp = r'^((?P<component>CA|SW)_MT\d+_(?P<name>.*)_lid-(?P<lid>0x\w*))()|,\w*,\d*_(?P<cable>.*)$'


    def init_metrics(self):

        for device in self.device_temp:
            self.metrics[device] = GaugeMetricFamily(
                'infiniband_' + device.lower(),
                self.device_temp[device]['help'],
                labels=[
                    'component',
                    'name',
                    'lid',
                    'guid'
                ]
            )

        for link in self.link_temp:
            self.metrics[link] = GaugeMetricFamily(
                'infiniband_' + link.lower(),
                self.link_temp[link]['help'],
                labels=[
                    'component',
                    'name',
                    'lid',
                    'guid',
                    'port'
                ]
            )

    def get_type(self, name):
        if re.match(r"pciconf\d+$", name.split("_")[-1]) :
            return ("pciconf")
        elif "Aggregation" in name.split("_") :
            return ("aggregation_node")
        elif "cable" in name.split("_"):
            return("cable")
        else : return "device"

    def temp_exporter(self):
        cmd = 'ls /dev/mst'
        cmd_output = subprocess.check_output(cmd.split())
        lines = cmd_output.decode().split("\n")
        #Check that correctly ran command return "" at the end
        if lines[len(lines) -1] == "":
            del lines[len(lines) -1]

        for line in lines:
            content = re.split(self.identify_link_temp, line)
            if len(content) == 1: #Check for inconsistent lines and ignore them
                logging.debug(f'Component not included because not manage by the program: {content[0]}')
                continue
            if content[0] == "":
                del content[0]
            else :
                raise ParsingError(f'Inconsistent input content detected: {format(content[0])}')

            #Set default name and guid values
            name=content[2]
            lid=content[3]
            guid=""
            if self.lid_to_name_map :
                with open(self.lid_to_name_map, 'r') as file:
                    datas = file.readlines()
                    for data in datas:
                        if content[3] in data:
                            name = data.split(" ")[2]
                            guid = data.split(" ")[1]

            if(self.get_type(line) == "device"):
                cmd = f'sudo mget_temp -d {line}'
                cmd_output = subprocess.check_output(shlex.split(cmd))
                res = cmd_output.decode().split("\n")
                for switch in self.device_temp:
                    label_values = [
                        content[1],
                        name,
                        lid,
                        guid]
                    self.metrics[switch].add_metric(label_values, res[0])

            elif(self.get_type(line) == "cable"):
                if content[1] == "CA":
                    continue
                cmd = f'sudo mlxcables -d {line}'
                cmd_output = subprocess.check_output(shlex.split(cmd))
                res = cmd_output.decode().split("\n")
                temp = res[-5].split(": ")
                try :
                    port = f"{int(content[-2].split('_')[-1]):02}"
                except ValueError:
                    logging.error(f"The port given is not an int : {content[-2].split('_')[-1]}")
                    port = "Unknown"
                for link in self.link_temp:
                    label_values = [
                        content[1],
                        name,
                        lid,
                        guid,
                        port]
                    self.metrics[link].add_metric(label_values, 0 if (temp[1] == "N/A") else temp[1])
            
            else : 
                logging.debug(f'Component not included because not manage by the program: {line}')    

    def collect(self):

        logging.debug('Start of collection cycle')

        self.scrape_with_errors = False
        
        scrape_duration = GaugeMetricFamily(
            'infiniband_scrape_duration_seconds',
            'Number of seconds taken to collect and parse the stats.')
        scrape_start = time.time()
        scrape_ok = GaugeMetricFamily(
            'infiniband_scrape_ok',
            'Indicates with a 1 if the scrape was successful and complete, '
            'otherwise 0 on any non critical errors detected '
            'e.g. ignored lines from ibqueryerrors STDERR or parsing errors.')

        self.init_metrics()

        try :
            self.temp_exporter()
        except ParsingError as e:
            logging.error(e)
            self.scrape_with_errors = True


        for device in self.device_temp:
            yield self.metrics[device]
        for link in self.link_temp:
            yield self.metrics[link]

        scrape_duration.add_metric([], time.time() - scrape_start)
        yield scrape_duration

        if self.scrape_with_errors:
            scrape_ok.add_metric([], 0)
        else:
            scrape_ok.add_metric([], 1)
        yield scrape_ok

        logging.debug('End of collection cycle')


# stolen from stackoverflow (http://stackoverflow.com/a/377028)
def which(program):
    """
    Python implementation of the which command
    """
    def is_exe(fpath):
        """ helper """
        return os.path.isfile(fpath) and os.access(fpath, os.X_OK)

    fpath, _ = os.path.split(program)
    if fpath:
        if is_exe(program):
            return program
    else:
        paths = os.getenv("PATH", "/usr/bin:/usr/sbin:/sbin:/bin")

        for path in paths.split(os.pathsep):
            exe_file = os.path.join(path, program)
            if is_exe(exe_file):
                return exe_file

    return None


class NoLoggingWSGIRequestHandler(WSGIRequestHandler):
    def log_message(self, format, *args):
        pass


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Prometheus collector for a infiniband fabric')
    parser.add_argument(
        '--port',
        type=int,
        default=9684,
        help='Collector http port, default is 9684')
    parser.add_argument(
        '--lid-to-name-map',
        action='store',
        dest='lid_to_name_map',
        help='lid-to-name-mad used by temp-exporter. Can also be set with env \
var LID_TO_NAME_MAP')
    parser.add_argument("--verbose", help="increase output verbosity",
                        action="store_true")
    parser.add_argument('-v',
                        '--version',
                        dest='print_version',
                        required=False,
                        action='store_true',
                        help='Print version number')

    args = parser.parse_args()

    if args.print_version:
        print(f"Version {VERSION}")
        sys.exit()

    if args.verbose:
        logging.basicConfig(level=logging.DEBUG,
                            format='%(asctime)s - %(levelname)s - %(message)s')
    else:
        logging.basicConfig(level=logging.INFO,
                            format='%(asctime)s - %(levelname)s - %(message)s')

    if args.lid_to_name_map:
        logging.debug('Using lid_to_name_map provided in args: %s', args.lid_to_name_map)
        lid_to_name_map = args.lid_to_name_map
    elif 'LID_TO_NAME_MAP' in os.environ:
        logging.debug('Using LID_TO_NAME_MAP provided in env vars: %s', os.environ['LID_TO_NAME_MAP'])
        lid_to_name_map = os.environ['LID_TO_NAME_MAP']
    else:
        logging.debug('No lid-to-name-map was provided')
        lid_to_name_map = None

    app = make_wsgi_app(InfinibandCollector(
        lid_to_name_map))
    httpd = make_server('', args.port, app,
                        handler_class=NoLoggingWSGIRequestHandler)
    httpd.serve_forever()
